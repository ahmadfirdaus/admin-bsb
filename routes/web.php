<?php


Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    
    Route::get('/', 'HomeController@index');
    // Route::get('categories', function () {
    //     return view('categorys.category');
    // });

    Route::group(['middleware' => ['admin']], function () {
        Route::resources([
            'categories' => 'CategoryController', 
            'membercat' => 'CategorysController',
        ]);
    });

    Route::resources([
        'products' => 'ProductController',
        'promos' => 'PromosController',
        'transactions' => 'TransaksiController',
        'members' => 'MemberController',
    ]);    

    Route::get('/home', 'HomeController@index')->name('home');
});