@extends('app')

@section('content')

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2> Update Member_category</h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown"></li>
                    </ul>
                </div>
                <div class="body">
                <div class="form-group">
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <form action="{{ route('membercat.update', $membercat->id) }}" method="post" autocomplete="off" class="form-group" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                                <div class="form-group">
                                  <label for="name">name</label>
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="name" id="name" value ="{{ $membercat->name }}">
                                    </div>
                                 </div>
                            <button type="submit" class="btn btn-primary">update</button>
                        </form>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection