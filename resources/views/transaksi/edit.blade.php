@extends('app')

@section('content')

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2> Update Transaksi</h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown"></li>
                    </ul>
                </div>
                <div class="body">
                <div class="form-group">
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <form action="" method="post" autocomplete="off" class="form-group" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                                <div class="form-group">
                                  <label for="trx_number">trx_number</label>
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="trx_number" id="trx_number" value ="{{ $transactions->trx_number }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="product_id">Promo</label>
                                        <div class="form-line">
                                        <select  class="form-control" name="product_id" id="product_id" required  value ="{{ $transactions->product_id }}">
                                        <option disabled selected>-- Pilih Satu --</option>
                                         @foreach($products as $pro)
                                         <option {{ $transactions->product_id == $pro->id ? "selected" : ""}} value="{{ $pro->id }}"> {{ $pro->name }} </option>
                                         @endforeach
                                         </select> 
                                         </div>
                                  </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label for="quantity">Quantity</label>
                                            <input type="text" id="quantity" name="quantity" class="form-control" required  value ="{{ $transactions->quantity }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="discount">discount</label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="discount" id="discount"  value ="{{ $transactions->discount }}" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="total">total</label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="total" id="total"  value ="{{ $transactions->total }}" >
                                        </div>
                                    </div>
                            <button type="submit" class="btn btn-primary">update</button>
                        </form>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->
    

@endsection