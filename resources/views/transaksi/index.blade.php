@extends('app')

@section('content')

<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                            Transaksi
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>no</th>
                                            <th>trx_number</th>
                                            <th>product</th>
                                            <th>quantity</th> 
                                            <th>discount</th>
                                            <th>total</th>
                                            <th>update</th> 
                                            <th>delete</th>                                          
                                        </tr>
                                    <tbody>
                                        @foreach ($transactions as $n => $transaction)
                                            <tr>
                                                <td>{{ $n+1 }}</td>
                                                <td>{{ $transaction->trx_number }}</td>
                                                <td>{{ $transaction->transactions ['name']}}</td>
                                                <td>{{ $transaction->quantity }}</td>
                                                <td>{{ $transaction->discount }}</td>
                                                <td>{{ $transaction->total }}</td>
                                                <td>
                                                <a href="{{ route('transactions.edit', $transaction->id) }}">
                                                    <button type="button" class="btn btn-primary">Ubah</button>
                                                </a>
                                                </td>
                                                <form action="{{ route('transactions.destroy', $transaction->id) }}" method="post"> 
                                                @csrf  
                                                @method('delete')                                         
                                                <td><button type="submit" class="btn btn-danger">delete</button></td>
                                                </form>   
                                            </tr>                                        
                                        @endforeach
                                    </tbody>
                                </table>
                                <button type="submit" data-toggle="modal" data-target="#defaultModal" class="btn btn-primary">Tambah</button>
                            </div>
                            <ul class="list-inline"></ul>   
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="defaultModal" tabindex="1" role="dialog">
	                        <div class="modal-dialog" role="document">
	                            <div class="modal-content">
	                                <div class="modal-header">
	                                    <h4 class="modal-title" id="defaultModalLabel" align="center" >Input Transactions</h4>
	                                </div>
	                                <div class="body">
	                                    <form action="{{ route('transactions.store') }}" method="post" id="simpanData" autocomplete="off" class="form-group">
	                                        @csrf
                                            <div class="form-group">
                                                <label for="product_id">Promo</label>
                                                <div class="form-line">
                                                    <select  class="form-control" name="product_id" id="product_id" required>
                                                    <option disabled selected>-- Pilih Satu --</option>
                                                        @foreach($products as $pro)
                                                            <option value="{{ $pro->id }}"> {{ $pro->name }} </option>
                                                        @endforeach
                                                    </select> 
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            	<div class="form-line">
                                            		<label for="quantity">Kuantitas</label>
                                            		<input type="text" id="quantity" name="quantity" class="form-control" required>
                                            	</div>
                                            </div>
	                                        <br><div class="modal-footer"><br>
	                                            <button type="submit" class="btn btn-link waves-effect">SAVE</button>
	                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
	                                        </div>

	                                    </form>  
	                                </div>
	                            </div>
	                        </div>

@endsection