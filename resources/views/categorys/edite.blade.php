@extends('app')

@section('content')

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Product Category
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                <div class="form-group">
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <form action="{{ route('categories.update', $category->id) }}" method="post" autocomplete="off" class="form-group">
                        @csrf
                        @method('PUT')
                                <div class="form-group">
                                  <label for="name">name</label>
                                  <div class="form-line">
                                  <input type="text" class="form-control" name="name" id="name" value="{{ $category->name }}">
                                  {!! $errors->first('name', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                  </div>

                                <div class="form-group">
                                  <label for="desc">desc</label>
                                  <div class="form-line">
                                  <input type="text" class="form-control" name="desc" id="desc" value="{{ $category->desc }}">
                                  {!! $errors->first('desc', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                  </div>
                            <button type="submit" class="btn btn-primary">update</button>
                        </form>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->
    

@endsection