@extends('app')

@section('content')

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Input Product
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                        </li>
                    </ul>
                </div>
                <div class="body">
                <div class="form-group">
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <form action="{{ route('products.store') }}" method="post" autocomplete="off" class="form-group" enctype="multipart/form-data">
                        @csrf
                                <div class="form-group">
                                  <label for="name">name</label>
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="name" id="name">
                                        {!! $errors->first('name', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="fallback">
                                        <label>Upload Photo</label>
                                        <input type="file" id="image" name="image" class="form_control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="product_category_id">category</label>
                                    <div class="form-line">
                                        <select  class="form-control" name="product_category_id" id="product_category_id">
                                            <option disabled selected>Pilih Satu</option>
                                            @foreach($categories as $cats)
                                                <option value="{{ $cats->id }}"> {{ $cats->name }} </option>
                                            @endforeach
                                        </select> 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                  <label for="desc">desc</label>
                                  <div class="form-line">
                                  <input type="text" class="form-control" name="desc" id="desc">
                                  {!! $errors->first('desc', '<span class="invalid-feedback">:message</span>') !!}
                                  </div></div>
                                  <div class="form-group">
                                  <label for="amount">amount</label>
                                  <div class="form-line">
                                  <input type="text" class="form-control" name="amount" id="amount">
                                  {!! $errors->first('amount', '<span class="invalid-feedback">:message</span>') !!}
                                  </div>
                                  </div>
                            <button type="submit" class="btn btn-primary">simpan</button>
                        </form>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->
    

@endsection