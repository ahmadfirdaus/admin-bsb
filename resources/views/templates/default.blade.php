@extends('app')

@section('content')

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Category Product
                                <small>Product name</small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table class="table"> 
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Desc</th>       
                                        <th>Update</th>       
                                        <th>Delete</th>                                    
                                    </tr>                                    
                                </thead>
                                <tbody>
                                    @foreach ($categories as $n => $category)
                                        <tr>
                                            <td>{{ $n+1 }}</td>
                                            <td>{{ $category->name }}</td>
                                            <td>{{ $category->desc }}</td>
                                            <td>
                                            <a href="{{ route('categories.edit', $category->id) }}">
                                                <button type="button" class="btn btn-primary">Ubah</button>
                                            </a>  
                                            </td> 
                                            <form action="{{ route('categories.destroy', $category->id) }}" method="post"> 
                                            @csrf     
                                            @method('delete')                                      
                                            <td><button type="submit" class="btn btn-danger" >delete</button></td>
                                            </form>   
                                        </tr>                                        
                                    @endforeach
                                </tbody>
                            </table>
                            <a href="categories"><button type="submit" class="btn btn-primary">Tambah</button></a>
                        </div>
                        <ul class="list-inline"></ul>      
                    </div>
                </div>
            </div>

            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               All Product
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>no</th>
                                            <th>name</th>
                                            <th>image</th>
                                            <th>category</th>
                                            <th>desc</th>
                                            <th>amount</th> 
                                            <th>update</th> 
                                            <th>delete</th>                                          
                                        </tr>
                                    </thead>     
                                    <tbody>
                                    @foreach ($products as $n => $product)
                                        <tr>
                                            <td>{{ $n+1 }}</td>
                                            <td>{{ $product->name }}</td>
                                            <td> <img src="{{ $product->image }}" height="80px" alt="gambar">  </td>
                                            <td>{{ $product->produc ['name']}}</td>
                                            <td>{{ $product->desc }}</td>
                                            <td>{{ $product->amount }}</td>
                                            <td>
                                            <a href="{{ route('products.edit', $product->id) }}">
                                                <button type="button" class="btn btn-primary">Ubah</button>
                                            </a>
                                            </td>
                                            <form action="{{ route('products.destroy', $product->id) }}" method="post"> 
                                            @csrf 
                                            @method('delete')                                          
                                            <td><button type="submit" class="btn btn-danger">delete</button></td>
                                            </form>   
                                        </tr>                                        
                                    @endforeach
                                    </tbody>
                                </table>
                                <a href="Product"><button type="submit" class="btn btn-primary">Tambah</button></a>
                            </div>
                            <ul class="list-inline"></ul>   
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                            Promo
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>no</th>
                                            <th>name</th>
                                            <th>product</th>
                                            <th>discount</th>
                                            <th>update</th> 
                                            <th>delete</th>                                          
                                        </tr>
                                    <tbody>
                                    @foreach ($promos as $n => $promo)
                                        <tr>
                                            <td>{{ $n+1 }}</td>
                                            <td>{{ $promo->name }}</td>
                                            <td>{{ $promo->promos ['name']}}</td>
                                            <td>{{ $promo->discount }}</td>
                                            <td>
                                            <a href="{{ route('promos.edit', $promo->id) }}">
                                                <button type="button" class="btn btn-primary">Ubah</button>
                                            </a>
                                            </td>
                                            <form action="{{ route('promos.destroy', $promo->id) }}" method="post"> 
                                            @csrf     
                                            @method('delete')                                      
                                            <td><button type="submit" class="btn btn-danger">delete</button></td>
                                            </form>   
                                        </tr>                                        
                                    @endforeach
                                    </tbody>
                                </table>
                                <a href="Promos"><button type="submit" class="btn btn-primary">Tambah</button></a>
                            </div>
                            <ul class="list-inline"></ul>   
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->


@endsection
@section('script')
@endsection
