@extends('app')

@section('content')

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Promo
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                        </li>
                    </ul>
                </div>
                <div class="body">
                <div class="form-group">
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <form action="{{ route('promos.store') }}" method="post" autocomplete="off" class="form-group" enctype="multipart/form-data">
                        @csrf
                                <div class="form-group">
                                  <label for="name">name</label>
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="name" id="name">
                                        {!! $errors->first('name', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="product_id">product</label>
                                    <div class="form-line">
                                        <select  class="form-control" name="product_id" id="product_id">
                                            <option disabled selected>Pilih Satu</option>
                                            @foreach($products as $prod)
                                                <option value="{{ $prod->id }}"> {{ $prod->name }} </option>
                                            @endforeach
                                        </select> 
                                    </div>
                                  </div>
                                    <div class="form-group">
                                    <label for="discount">discount</label>
                                    <div class="form-line">
                                    <input type="text" class="form-control" name="discount" id="discount">
                                    {!! $errors->first('discount', '<span class="invalid-feedback">:message</span>') !!}
                                    </div>
                                    </div>
                            <button type="submit" class="btn btn-primary">simpan</button>
                        </form>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Table -->
    

@endsection