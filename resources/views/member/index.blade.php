@extends('app')

@section('content')

<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                            Member
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>no</th>
                                            <th>category</th>
                                            <th>full name</th>
                                            <th>dob</th> 
                                            <th>address</th>
                                            <th>gender</th>
                                            <th>barcode</th>
                                            <th>update</th> 
                                            <th>delete</th>                                          
                                        </tr>
                                    <tbody>
                                        @foreach ($members as $n => $member)
                                            <tr>
                                                <td>{{ $n+1 }}</td>
                                                <td>{{ $member->members ['name']}}</td>
                                                <td>{{ $member->full_name }}</td>
                                                <td>{{ $member->dob }}</td>
                                                <td>{{ $member->address }}</td>
                                                <td>{{ $member->gender }}</td>
                                                <td>{{ $member->barcode }}</td>
                                                <td>
                                                <a href="{{ route('members.edit', $member->id) }}">
                                                    <button type="button" class="btn btn-primary">Ubah</button>
                                                </a>
                                                </td>
                                                <form action="{{ route('members.destroy', $member->id) }}" method="post"> 
                                                @csrf 
                                                @method("delete")                                          
                                                <td><button type="submit" class="btn btn-danger">delete</button></td>
                                                </form>   
                                            </tr>                                        
                                        @endforeach
                                    </tbody>
                                </table>
                                <button type="submit" data-toggle="modal" data-target="#defaultModal" class="btn btn-primary">Tambah</button>
                            </div>
                            <ul class="list-inline"></ul>   
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="defaultModal" tabindex="1" role="dialog">
	            <div class="modal-dialog" role="document">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <h4 class="modal-title" id="defaultModalLabel" align="center" >Input Member</h4>
	                    </div>
                        <div class="body">
	                            <form action="{{ route('members.store') }}" method="post" id="simpanData" autocomplete="off" class="form-group">
	                                @csrf
                                <div class="form-group">
                                    <label for="member_category_id">Kategori</label>
                                        <div class="form-line">
                                            <select  class="form-control" name="member_category_id" id="member_category_id" required>
                                                <option disabled selected>-- Pilih Satu --</option>
                                                @foreach($membercat as $member)
                                                <option value="{{ $member->id }}"> {{ $member->name }} </option>
                                                @endforeach
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label for="full_name">Nama Lengkap</label>
                                            <input type="text" id="full_name" name="full_name" class="form-control" required>
                                        </div>
                                     </div>
                                     <div class="form-group">
                                        <div class="form-line"><br>
                                            <label for="dob">Tanggal</label>
                                            <input type="date" id="dob" name="dob" class="form-control" required>    
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label for="address">Alamat</label>
                                            <input type="text" id="address" name="address" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    <label for="gender">Jenis kelamin</label>
                                        <div class="demo-radio-button">
                                            <input type="radio" name="gender" id="laki-laki" value="laki-laki" class="with-gap">
                                            <label for="laki-laki">Laki-laki</label>
                                            <input type="radio" name="gender" id="perempuan" value="perempuan" class="with-gap">
                                            <label for="perempuan">Perempuan</label>
                                        </div>
                                    </div>
	                                <br><div class="modal-footer"><br>
	                                    <button type="submit" class="btn btn-link waves-effect">SAVE</button>
	                                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
	                                    </div>
	                            </form>  
	                        </div>
	                </div>
	             </div>


@endsection