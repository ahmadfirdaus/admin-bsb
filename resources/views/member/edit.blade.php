@extends('app')

@section('content')

<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2> Update Member</h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown"></li>
                    </ul>
                </div>
                <div class="body">
                <div class="form-group">
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <form action="{{ route('members.update', $member->id) }}" method="post" autocomplete="off" class="form-group" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                                    <div class="form-group">
                                    <label for="member_category_id">Category</label>
                                        <div class="form-line">
                                            <select  class="form-control" name="member_category_id" id="member_category_id" required>
                                                <option disabled selected>-- Pilih Satu --</option>
                                                @foreach($membercat as $membr)
                                                <option {{ $member->member_category_id == $membr->id ? "selected" : "" }} value="{{ $membr->id }}">{{ $membr->name }}</option>

                                                @endforeach
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label for="full_name">Full name</label>
                                            <input type="text" id="full_name" name="full_name" class="form-control" required value ="{{ $member->full_name }}">
                                        </div>
                                     </div>
                                     <div class="form-group">
                                        <div class="form-line"><br>
                                            <label for="dob">DOB</label>
                                            <input type="date" id="dob" name="dob" class="form-control" required value ="{{ $member->dob }}">    
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label for="address">Address</label>
                                            <input type="text" id="address" name="address" class="form-control" required value ="{{ $member->address }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="demo-radio-button">
                                            <input type="radio" name="gender" id="laki-laki" value="laki-laki" class="with-gap">
                                            <label for="laki-laki">Laki-laki</label>
                                            <input type="radio" name="gender" id="perempuan" value="perempuan" class="with-gap">
                                            <label for="perempuan">Perempuan</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label for="barcode">Barcode</label>
                                            <input type="text" id="barcode" name="barcode" class="form-control" required value ="{{ $member->barcode }}">
                                        </div>
                                    </div>
                            <button type="submit" class="btn btn-primary">update</button>
                        </form>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection