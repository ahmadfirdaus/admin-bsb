<?php

namespace App\Http\Controllers;
use App\Category;
use Illuminate\Http\Request;


class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('categorys.category', compact('category', 'categories'));
    }
    public function store()
    {
        $categories = new Category();   
        $categories->name = request('name');
        $categories->desc = request('desc');
        $categories->save();

        return redirect('/');
       
    }
    public function destroy(Category $categories , $id )
    {
        $categories = Category::findOrFail($id);
        $categories->delete();

        return back();
    }
    public function edit(Category $category)
    {
        
        return view('categorys.edite' , compact('category'));
    }
    public function update(Request $request, Category $categories, $id)
    {
      
        $request->validate([
            'name' => 'required',
            'desc' => 'required',

    
            ]);
        $categories = Category::findOrFail($id);
        $categories->name = $request->name;
        $categories->desc =  $request->desc;
        $categories->update();
            // $categories->save();
        
       
        return redirect("/");
    }

}
