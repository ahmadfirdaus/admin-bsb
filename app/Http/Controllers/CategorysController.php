<?php

namespace App\Http\Controllers;
use\App\Categorys;
use Illuminate\Http\Request;

class CategorysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $membercat = Categorys::all();
       return view('membercat.index', compact('membercat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $membercat = new Categorys();   
        $membercat->name = request('name');
        $membercat->save();

        return redirect('/membercat');
           }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Categorys $membercat)
    {
        return view('membercat.edit' , compact('membercat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categorys $categorys, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $data = Categorys::findOrFail($id);
        $data->name = $request->name;
        $data->update();
        //    return response()->json($data);

        return redirect('membercat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categorys $categorys, $id)
    {
        $category = $categorys::findOrFail($id);
        $category->delete();
        return redirect('membercat');   
    }
}
