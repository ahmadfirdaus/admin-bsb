<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\table_promos;
use App\table_product;

class PromosController extends Controller
{
    public function index()
    {
        $products = table_product::all();
        $promo = table_promos::all();
        return view('promo.promos', compact('products', 'promo'));
    }
    public function store()
    {
        request()->validate([
        'name' => 'required',
        'product_id' => 'required',
        'discount' => 'required',

        ]);

        $promos = new table_promos();
        $promos->name = request('name');
        $promos->product_id = request('product_id');
        $promos->discount = request('discount');
        $promos->save();

        return redirect('/');

    }
    public function edit(table_promos $promo)
    {
        $products = table_product::all();
        return view('promo.edit' , compact('products','promo'));
    }

    public function update(Request $request, $id )
    {
        $request->validate([
            'name' => 'required',
            'product_id' => 'required',
            'discount' => 'required',
        ]);

        $promo = table_promos::findOrFail($id);
        $promo->name = $request->name;
        $promo->product_id = $request->product_id;
        $promo->discount = $request->discount;
        $promo->update();
        
        return redirect('/');
    }

    public function destroy(table_promos $promos, $id)
    {
        $promos = table_promos::findOrFail($id);
        $promos->delete();

        return back();
    }
}
