<?php

namespace App\Http\Controllers;
use\App\table_product;
use\App\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
   public function index()
   {
    $products = table_product::all();
    $transactions = Transaksi::all();
    return view('transaksi.index', compact('products', 'transactions'));  
   }

   public function store(Request $request)
   {
       $request->validate([
           'product_id' => 'required',
           'quantity' => 'required',
       ]);


       //trx number generator
       $rmdash = str_replace("-","",Carbon::now()->toDateTimeString());
       $rmcolon = str_replace(":","", $rmdash);
       $rmspace = str_replace(" ","",$rmcolon).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
       $trx_number = 'TRX'.$rmspace;

       $init = table_product::where('id', $request->product_id)->get()->first()->amount;

       $total = $request->quantity * $init;
       $transactions = new Transaksi();
       $transactions->trx_number = $trx_number;
       $transactions->product_id = $request->product_id;
       $transactions->quantity = $request->quantity;
       $transactions->discount = 0;
       $transactions->total = $total;
       $transactions->save();
       // return response()->json($transactions);
       return back();
   }

   public function edit(Transaksi $transactions)
   {
       $products = table_product::all();
       return view('transaksi.edit' , compact('products','transactions'));
   }

   public function update(Request $request, $id )
   {
       
       $request->validate([
           'trx_nuumber'       => 'required',
           'product_id' => 'required',
           'quantity' => 'required',
           'discount'   => 'required',
           'total' => 'required',

       ]);

           $transactions = Transaksi::findOrFail($id);
           $transactions->trx_number =  $request->trx_number;
           $transactions->product_id =  $request->product_id;
           $transactions->quantity =  $request->quantity;
           $transactions->discount =  $request->discount;
           $transactions->total =  $request->total;
           $transactions->update();
        //    return response()->json($request);
      
       return redirect('/transactions');
   }


   public function delete(Transaksi $transactions)
   {
       $transactions->delete();

       return back();
   }
}
