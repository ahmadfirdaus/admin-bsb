<?php

namespace App\Http\Controllers;
use App\Category;
use App\table_product;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        
        $products = table_product::all();
        $categories = Category::all();
        return view('product.produc', compact('products', 'categories'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'name'  =>  'required',
            'desc'  =>  'required',
            'amount' => 'required',
            'product_category_id'  =>  'required',
            'image'  =>  'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->has('active')) {
           $active = 1;
        } else {
            $active = 0;
        }

        $fileName = str_replace("=","",base64_encode($request->name.time())) . '.' . request()->image->getClientOriginalExtension();

        if(!$request->image->move(storage_path('app/public/promo'), $fileName)){
            return array('error' => 'Gagal upload foto');
        } else {
            $product = new table_product ();
            $product->name = $request->name;
            $product->desc = $request->desc;
            $product->amount = $request->amount;
            $product->product_category_id = $request->product_category_id;
            $product->image = "storage/promo/".$fileName;;
            $product->save();
        }

        return redirect('/');
       
    }
    public function destroy(table_product $product)
    {
        $product->delete();

        return back();
    }
    public function edit(table_product $product)
    {
        $categories = Category::all();
        return view('product.edits' , compact('categories','product'));
    }
    public function update(Request $request, $id)
    {
        $productPict = table_product::where("id","=",$id)->get()->first()->image;
        // return response()->json($productPict);
        if (!$request->image) {
            $request->validate([
                'name'  =>  'required',
                'product_category_id'  =>  'required',
                'desc'  =>  'required',
                'amount' => 'required',
            ]);
        } else {
            // return 'changed';
            $request->validate([
                'name'  =>  'required',
                'product_category_id'  =>  'required',
                'desc'  =>  'required',
                'image'  =>  'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'amount' => 'required',
            ]);
            $fileName = str_replace("=","",base64_encode($request->name.time())) . '.' . request()->image->getClientOriginalExtension();
        }

        if ($request->has('active')) {
            $active = 1;
         } else {
             $active = 0;
         }

        $product = table_product::findOrFail($id);
        $product->name = $request->name;
        $product->desc = $request->desc;
        $product->product_category_id = $request->product_category_id;
        $product->amount  = $request->amount;
        if($request->hasFile('image')){
            // return 'has file';
            if (is_file($product->image)) {
                try {
                    unlink($productPict);
                } catch(\Exception $e) {

                }
            }
            $request->image->move(storage_path('app/public/promo'), $fileName);
            $product->image = "storage/promo/".$fileName;
        }else {
            // return 'kosong';
            $product->image = $productPict;
        }
        $product->save();

        return redirect("/");
    }
   
}
