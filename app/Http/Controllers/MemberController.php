<?php

namespace App\Http\Controllers;

use App\Member;
use App\Categorys;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::all();
        $membercat = Categorys::all();
        return view('member.index', compact('members', 'membercat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'member_category_id'  =>  'required',
            'full_name'  =>  'required',
            'dob' => 'required',
            'address' => 'required', 
            'gender' => 'required', 
          
        ]);

       //trx number generator
       $rmdash = str_replace("-","",$request->dob);
       $rmcolon = str_replace(":","", $rmdash);
       $rmspace = str_replace(" ","",$rmcolon).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
       $barcode = 'BC'.$rmspace;


        $members = new Member ();
        $members->member_category_id = $request->member_category_id;
        $members->full_name = $request->full_name;
        $members->dob = $request->dob;
        $members->address = $request->address;
        $members->gender = $request->gender;
        $members->barcode = $barcode;
                   
        $members->save();

    return redirect('members');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        $membercat = Categorys::all();
        return view('member.edit' , compact('member','membercat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {        
        $request->validate([
            'member_category_id'  =>  'required',
            'full_name'  =>  'required',
            'dob' => 'required',
            'address' => 'required', 
            'gender' => 'required', 
            'barcode' => 'required',
 
        ]);

        // $member = Member::findOrFail($id);
        $member->member_category_id = $request->member_category_id;
        $member->full_name = $request->full_name;
        $member->dob = $request->dob;
        $member->address = $request->address;
        $member->gender = $request->gender;
        $member->barcode = $request->barcode;
        $member->update();
        // return response()->json($member);
       
        return redirect('members');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        $member->delete();
        return redirect('members');   
    }
}
