<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class table_product extends Model
{
    protected $fillable = ['name','image','product_category_id','desc','amount'];
    public function produc()
    {
        return $this->belongsTo(Category::class, 'product_category_id');

    }
    public function promo()
    {
        return $this->hasMany(table_promo::class, 'product_id');

    }
    public function transactions()
    {
        return $this->hasMany(Transaksi::class, 'product_id');

    }
}
