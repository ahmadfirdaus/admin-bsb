<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;


class Member extends Model
{
    public function members()
    {
        return $this->belongsTo(Categorys::class, 'member_category_id'); 
    }
    protected $table = 'members';
    protected $guarded = ['id'];
    use SoftDeletes;
}
