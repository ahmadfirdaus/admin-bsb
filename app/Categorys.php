<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorys extends Model
{
    public function members()
    {
        return $this->hasMany(Member::class, 'member_category_id'); 
    }
    protected $table = 'member_categories';

}
