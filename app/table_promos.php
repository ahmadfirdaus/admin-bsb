<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class table_promos extends Model
{
    protected $guarded = ['id'];
    public function promos()
    {
        return $this->belongsTo(table_product::class, 'product_id'); 
    }
}
