<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    public function transactions()
    {
        return $this->belongsTo(table_product::class, 'product_id'); 
    }
    protected $table = 'transactions';
}
